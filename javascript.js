console.info('javascript loaded');

const cube = document.querySelector('.square');
const ctn = document.querySelector('.squareCtn');

function moveCubeTo(options) {
  console.info('function is called');

  let direction = options.direction, distance = options.distance;

  let startTime = Date.now();

  let timer = setInterval(() => {
    console.info('interval starts');
    let passedTime = Date.now() - startTime;

    if (passedTime >= (distance * 5.1)) {
      console.info('clear interval');
      clearInterval(timer);
      return;
    }


    cube.setAttribute(`style`, `${direction}:${ passedTime / 5 + 'px'}`);
  }, 1);


}


moveCubeTo({
  direction: 'top',
  distance: 200
});
